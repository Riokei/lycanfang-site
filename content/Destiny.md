+++
title = "Destiny 2"
+++
## Destiny 2 - "Invicta Furore"

The Lycanfang’s Destiny 2 team plays on PC, and is extremely active primarily on NA servers. Fronted by the Team Leader Amidaus, we are a PvP / PvE clan focused on pursing excellence in all aspects of the game, while having a fun time doing it. We look for new and old players who are looking for a gaming community to join. We have a discord that services multiple games.

#### What we can provide you:
>*  Weekly raids to farm for powerful gear from all it's sources in the game.
>* Regular completion of milestones of all kinds, with Sherpa's for innexperienced raiders, and Calm experienced PvPer's to help you get better.
>*  An active clan of roughly 90 members, a considerable amount of them being extremely knowledgeable in the workings of the game.

#### Long Term Goals
>* To provide both competitive and relaxed play to the players looking for an air of competition or the relaxing sensation of company.
>* To ensure players who are PvP and/or PvE always have a group to run with including but not limited to;
>* A raid team dedicated to being first into the new raids and raid lairs. (Including Prestige runs)
>* A seperate raid team dedicated to being patient with new raiders ensureing the clan recieves its milestones and the satisfaction of killing bosses and finishing encounters.
>* A group willing to assist with strikes and the Nightfall (including possible Prestige runs)
>* Ensuring that all players who come to Lycanfang feel welcome and always have clanmates to play the game with who are active and enjoy helping each other.

Lycanfang is a PvE/PvP clan as stated before multiple times. As so we are dedicated to completing all milestones, daily challenges, and especially running raids in Destiny 2. We have guided several new raiders found via LFG (looking for group) websites, or the guided games feature in game through the raids in Destiny 2. Who then decided to stay with the clan and play together after the amount of fun they had playing with us.

Current raid teams are ran by several of the moderators in the team. Reife, a head moderator of the Destiny 2 LFG discord server, and one of our "Vanguard" (The head moderation of the Destiny 2 Team) takes all comers and gladly does runs with either seasoned veterans or new players a like. MiniHero, is a player that joined us in year 2 of Destiny 2 and quickly found himself through the ranks for his unmatched enthusiasm to do hard activities, learn how to do high level raid encounters, and overall be a better player. He actively attempts to teach new players how to do things. While we do usually run raids on Reset Day (every Tuesday at 10am Pacific,) we encourage all members of the clan to actively seek out groups with each other to get clears done throughout the week on their characters. There's no limitation of course. Ensure that you're being polite in your asking and you'll almost assuredly get a group in no time.

#### How to Join
You can join us on [Discord](https://discord.gg/gMv4ebh) and this will take you to the main Hub of Lycanfang! There's a [separate server](https://discord.gg/3EtKA2A) to accommodate the Destiny 2 Team, so just ask for a link once you're there! From there contact Amidaus to invite you to the destiny team when he’s online. A note for those joining: While we’d prefer you have the DLC its not required to join!
Contact Amidaus on
Battle.net/in-game (Amidaus#1660)
Discord (Amidaus#5358)
Reddit (Amidaus)

From all of us in The Lycanfang Destiny 2 team, we hope to see you in The Tower. Good luck out there Guardians. - Amidaus, Destiny 2 Team Lead.


{{< load-photoswipe >}}
{{< gallery dir="img/d21" hover-effect="grow" />}}
