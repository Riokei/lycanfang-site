+++
title = "Code of Conduct"
description = "Code of Conduct"
keywords = ["Rules"]
+++

Not following these rules could result in a punishment or worst: a ban. Please understand that the rules exist to keep our atmosphere in this community one that is welcoming and relaxed. We're pretty toleratant of people, don't abuse our tolerance.

## 1. No trolling, inflammatory behavior, or just generally being a butt for the sake of it.

Flaming, trolling, arguing for the sake of arguing, excessive cursing, violent/gore and 18+ content is banned. You post stuff like that you get kicked or muted til you learn your lesson and if you still don't you will be banned.  

## 2. Don't spam.

This includes bot created spam from using bot commands, we have a specific section for using bot commands: if its Tatsumaki use #bot-spam, if its a more specific bot for a specific team use that team's channel to use the bot. (Like the GW2Bot in the #gw2 channel.)

## 3. No bigotry.

Here on this wonderful invention called the internet: you will encounter people of different beliefs, ideaologies, religions, sexualities, genders, etc etc. Try to be respectful and understanding and be willing to edit or remove a joke or not complain if a joke/meme/comment whatever gets removed if deemed offensive.

## 4. Be respectful towards your mods and members.
Respect both the authority of mods and your fellow members! Try to get along and don't escalate and troll for the sake of it!


## 5. Advertising

Please do not advertise other discord servers or guilds/communities.

---

> If any of these rules need clarifying please let one of the mods/Death Jackals know.
