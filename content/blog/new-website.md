+++
title = "New Website"
date = "2017-12-08T13:07:31+02:00"
tags = ["website, guild updates,"]
categories = ["website"]
banner = "img/banners/banner-6.jpg"
author = "Rioku"
+++

## Welcome to our new website!
This new website is now powered by [Hugo](https://gohugo.io/) and hosted on [GitHub](https://github.com/)! Our old site was a forum powered by Zetaboards forum software but at this point we use discord more then our forums, so the old forums is just deadweight at this point. We needed something more simple and easy to maintain as a nice static advertisement and information center for our community.

New to our community? Not sure what we're about? Time to go into advertising mode! LycanFang is a casual gaming community originally based on Artix Entertainment's game Adventure Quest Worlds. We've since branched out and moved on to other games but we still play AE's new game Adventure Quest 3D from time to time and continue to keep an eye on its development.
Our new games are as follows:


>*  Destiny 2 (PC version)
>* Guild Wars 2
>* Minecraft (We have a server!)
>* Warframe
>* Xbox One games like Destiny 2 (So you can have twice the destiny 2!!), Forza, Dying Light and more!


## Stay tuned!
We've got more updates coming as we work out how we set up individual pages for our teams! 
