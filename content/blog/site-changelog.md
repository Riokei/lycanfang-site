+++
title = "Change Log"
date = "2017-12-17"
tags = ["website, guild updates,"]
categories = ["website"]
banner = "img/banners/banner-7.jpg"
author = "Rioku"
+++

## Site Change Log
Read below to view all changes:

>* Seperated mega team page into individual team pages.
>* Updated Destiny 2 page with more information.
>* Updated Minecraft page to reflect the eventual shutdown of our server and move to Hypixel.
>* Fixed PhotoSwipe again.
>* Updated warframe team page to be clearer.
>* Updated the calendar.
