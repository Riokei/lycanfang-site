+++
title = "GW2 Raid team success!"
date = "2017-12-19"
tags = ["gw2 team,"]
categories = ["teams"]
banner = "img/banners/banner-8.jpg"
author = "Rioku"
+++

## Three raids in one night!

The GW2 team had a succesful Cairn + Mursaat Overseer + Samarog boss run on the tenth! We had about 5 or so from our guild and had to pug the rest, but overall we ran into some really nice people. We all had a great time learning and killing bosses together. Please enjoy our footage and pictures from the event:
{{< youtube jQWijXSj9uE >}}


We were not unfortunately able to capture audio, we'll try to aim for that next event! Unless you guys would rather we stick to just putting music over it, let us know in the comments or on discord. :)

{{< load-photoswipe >}}
{{< gallery dir="img/gw2-raid1" >}}
