---
title: "Febuary Updates"
date: 2018-02-12T11:25:59-05:00
draft: false
tags: ["gw2 team, community"]
categories: ["teams, community"]
banner: "img/banners/banner-7.jpg"
author: "Rioku"
---

## Quick Re-cap for January

In January we saw the gw2 team complete several raids including Bandit Trio (with a attempt at Matthias in which we got smacked down but, we will destroy him someday!) which we posted a video of here:
{{< youtube X64OoFWkOdU >}}


Outside of that:


>*  the D2 team steadily gained members, has numerous raids, and is doing great in pvp.
>*  the GW2 team added a new event night on wednesdays thanks to our new event manager Sacvendeozirosa(#6512)
>*  the Hypixel team has also gained members and plans more things (04DYS keeping us in suspense) in the future
>*  the warframe team also gained some members

## Birthdays
Please wish our leap year boi Thadei a happy birthday on the 28th (tho its actually on the 29th and all that) if anyone else is having a birthday this month: Happy birthday!!

## First Prestige Clear (D2)

Congratz to Amidaus, LordRhyolith, AllowMei, DeadlyMouse5, Abel, Aquilvo on their prestige clear in Destiny 2! And congratulations to Kemper for hitting the top 500 in the world in Countdown!
{{< gallery dir="/img/febblog/" />}}


## Staff meeting concluded!

We've changed around the order of our channels putting each game in its own category in order to support more news channels and voice channels for each team as they grow better. We've also decided to go ahead and remove the Xbox One team as we didn't have very people keeping that channel going. We added a music channel for you to share your favorite songs! It compliments our art channel.



{{< load-photoswipe >}}
