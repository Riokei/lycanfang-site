---
title: "Video Roundup of GW2 Team Raids and other Fun Stuff"
date: 2018-04-15T10:38:33-04:00
tags: ["gw2 team,"]
categories: ["teams"]
banner: "img/banners/banner-8.jpg"
author: "Rioku"
draft: "false"
---
#### All our videos for this year so far!
We hope you enjoy this roundup of all our videos this year so far.

{{< youtube EM2EK7HjfXY >}}

Sometime we do *insert class* only raids. This is from our all thief run back in December!

{{< youtube J0B5nRfSCoI >}}

We didn't get the kill this time thanks to a combination of things but enjoy watching us fail!

{{< youtube ZsWHYoNyfgQ >}}

Pug shenenigans and REVENGE!!! Also wing four.


{{< youtube N3aRk0uvlEA >}}

The team does a no armor+first person view run at Cairn and appears to have forgotten our awesomness at Cairn from the last video. Also Iiiiiiiiiiiiiiiiiiii willll alllways loooooove youuuuuu *skip 6 minutes in for that reference*

{{< youtube oxYJKP-d8Lc >}}

Deimos! ....and boneless pizza.

{{< youtube ZOn66rhCNb8 >}}

Escort is fun! KC not so much.

{{< youtube mwM1W3TeBr8 >}}

We finally killed sloth yay!
