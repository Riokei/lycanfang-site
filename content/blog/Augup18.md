+++
title = "August Updates and Silliness"
date = "2018-08-18"
tags = ["community,"]
categories = ["teams, community"]
banner = "img/banners/banner-7.jpg"
author = "Rioku"
+++

## Team and Staffing updates.
>* Drachen/NightWalker's computer died! He'll be out indefinitely until a new one can be gained. (GW2 Team)
>* Dreth has been promoted to territory staff and become an officer for the GW2 Team!
>* The Destiny 2 team grew so big they created their own discord server and raid hub. You can join the server here: https://discord.gg/w3xVtQB
>* GW2 Team now has a dedicated Fashion Wars 2 channel for all your fashion wars needs. :)
>* Selfie channel is now Selfies-and-Cats! Please post your feline (or other furry/feathered friend) there if you'd like to share!

## GW2 Team Nonsense - Aka The Salad Incident
After exiting a raid Kamikampi, Dreth and I (Rioku/Tired Danchou) stumbled across CrazyMonkeyMcgee afk in the Aerodrome. After thinking a minute we decided to build an afk shrine, and this is the result. Coleslaw (basically salad) in front of our salad.
{{< gallery dir="/img/notmysalad/" />}}

## More GW2 Team Nonsense
In another lovely moment from recent days while waiting for our raid to start Kamikampi went map breaking and tried gliding out the red glowing eye in the Aerodrome. Upon death he noticed his character still doing the breathing animation (o.O). While I attempted to catch up to him he captured this video:

{{< youtube LhJel4tTnU4 >}}


## Hope to catch you next update for more randomness!
If you have any suggestions for our website, or discord please let us know in the #suggestions-questions channel on our discord server. :) Thanks for reading!

{{< load-photoswipe >}}
