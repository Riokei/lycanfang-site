+++
title = "Small Updates and a Contest for Oct!"
date = "2018-10-16"
tags = ["community,"]
categories = ["teams, community"]
banner = "img/banners/banner-10.jpg"
author = "Rioku"
+++

### Screenshot Contest in the Guild Wars 2 Team!

Halloween in Guild Wars 2 starts today Oct 16th, and the staff team for our gw2 guild has put together a screenshot contest for our members! :) Please read the details of the contest and how to enter [here](https://docs.google.com/document/d/1-mcI3_85dI9nptuQL8KwbwpXRg51fO5gS0gNrOAOZ5s/edit?usp=sharing) (note this is taking place on discord).

### Also related:
* While the Destiny 2 team will not be having a halloween contest/event the in-game event starts today!
* We've changed our server icon on discord to be a bit more spooky. :)

### Unrelated site updates:
* I've done some spell checking on the D2 page!
* We've changed our discord invite link as we have a new #welcome channel, with a bot giving member roles and team roles via reactions. :)
