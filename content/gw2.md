+++
title = "Guild Wars 2"
+++
{{< load-photoswipe >}}

## Guild Wars 2
![gw2teamlogo]({{< static "img/gw2.png" >}})

#### About Us
LycanFang is an older guild that moved to Guild Wars 2 in 2016. Based in the NA servers, we’re casual and lean towards a more relaxed atmosphere and playstyle. We have a group of people that play regularly and does raids weekly. Then there are the random events when people feel like it, such as only thief run dungeon. Sometimes even the WvW ones when we have enough people. (Eredon Terrace if you want to join us for those too.)
#### What we're looking to recruit:

>*  Newbies - We’d love to help you grow and love the game and can help give you a bit of advice.
>*  Returning players - See above, and of course we can try to help you get on your feet again if you missed some releases and want advice and help.
>*  Raiders - Our main weekly events are raids. So more raiders only means more availability to clear wings (And sweet rewards).
>*  People who don't mind using discord - Seriously, we use discord a lot in-between obviously being in-game and voice chat isn’t required we really only use it for something important like a special event where we need it (new people learning a fractal, raiding etc)


#### Events
The main events are raiding 2 or 3 times a week (Check the Calendar for the exact schedule). Then we do guild missions, some fractals, and dungeons. The dungeons are special since more often than not someone convinces the rest that we are only allowed to play one specific class for that run. Not very balanced, but fun.

Unless stated otherwise, all events start at 5:00 pm EST. We’ll ping you on those days exactly what time and if the event is going to take place. If you’ve been in the guild a little bit and would like to have an event slot we have an event manager position just below officer just for hosting events. (i.e Raids, Fractal runs etc, this mainly applies to your permissions on discord)
#### Guild Hall
A bit of info about that we have Gilded Hollow and have reached level 48.

#### Long Term Goals
>* To cultivate a nice solid group of people able to participate in our events and fully upgrade our guild hall. (Getting there!)
>* Decorate the entire guild hall. We currently have two scribes working on it. Some lounges, trophy area, and beetle racetrack is already done.
>* A large solid raid group we don’t have to pug those last few for!
>* Get ourselves a nice t3/t4 fractal group.

#### Repping Rules
We’re fine with you repping at least during guild events only if you would prefer that.
#### How to Join
You can mail in-game at Rikou.4720 or Kamikampi-1207, but it’d be great if you’d hop straight to our discord first if I’m not in-game. We have tons of helpful staff who can get you set up and that’ll give an officer a bit of time to add you. Our officers at this time (may update later) are: Dreth.9503, and NightWalker Kanna.9680. Again if you can’t get in touch with anyone quickly in-game please join us on discord.

{{< gallery dir="img/gw2" hover-effect="grow" />}}
